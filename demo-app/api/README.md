# Movie App (api)

This is the backend api service of the movie app for demo. 
It was a node.js app built using typescript.

It is simply a wrapper around public Omdb Movie API that provide movie data. 

## Local Development Machine

### Development

To run the app in local machine, ensure you have the following installed:
- Node.js and NPM
- yarn

At the `api` root directory, run the following:

```bash
# Pull and install dependencies and/or node packages
yarn

# The app expect both http port and omdb movie api key is provided
# as environment variables 
export API_KEY=XXXXX
export PORT=3000

# For windows machine, use the following command 
# instead to set environment variables 
# (note that it only last for current session)
set API_KEY=XXXX
set PORT=3000 

# This will start up nodemon (for hot-reload) and run the app
yarn dev
```
### Build

To build the app, run `yarn build`. This generate transpiled javascript code that can be consumed by `node`.


