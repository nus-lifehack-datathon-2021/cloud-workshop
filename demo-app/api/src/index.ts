import express, { Application, Request, Response, NextFunction } from 'express'
import Routes from './Routes'

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config();
}

// retrieve configurations from os environment variables 
const httpPort = process.env.PORT
const apiKey = process.env.API_KEY || ""

const app: Application = express();

// base api url (indicating api is running)
app.get('/', (req: Request, resp: Response) => {
    resp.send('movie app (api) is running')
});

// configure app or api routes
Routes({ app, apiKey })

app.listen(httpPort, () => {
    console.log(`server running on port ${httpPort}`);
});

