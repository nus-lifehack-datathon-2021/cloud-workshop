import { Application, Response } from 'express'
import MovieController, { MovieSnippet } from './controllers/MovieController'

// define custom route input that takes in 
// express application object for configuation  
export type RouteInput = {
    app: Application, 
    apiKey: string,     // omdb movie api key
}

function convertToHttpError(response: Response, error: any) {
    console.log(error);
    response.type('text/plain');
    response.status(500);
    response.statusMessage = error
    response.send(error);
}

// api router
export default({ app, apiKey }: RouteInput) => {

    const movieController = new MovieController(apiKey);

    app.get("/api/movie", async (req, resp) => {
        try {
            if(req.query.search) {
                const searchTerm: string = String(req.query.search)
                const data = await movieController.searchMovies(searchTerm)
                return resp.send(data);    
            } else {
                const data = await movieController.getDefaultMovieList()
                return resp.send(data);
            }
        } catch(error) {
            convertToHttpError(resp, error);
        }
    });
    app.get("/api/movie/:id", async (req, resp) => {
        try {
            const movieId = req.params.id
            const data = await movieController.getMovieDetail(movieId)
            return resp.send(data);
        } catch(error) {
            convertToHttpError(resp, error);
        }
    });
}