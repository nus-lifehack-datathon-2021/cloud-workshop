import { title } from 'process';
import OmdbMovieApi from './OmdbMovieApi' 

export interface MovieSnippet {
    title: string
    year: string
    id: string
    posterUrl: string
}

export interface Movie {
    title: string
    year: string
    parentalAdvice: string
    duration: string
    genre: string
    director: string
    writer: string
    actors: string
    plot: string
    language: string
    country: string
    posterUrl: string
    metascore: string
    rating: string
    id: string
    dvd?: string
    boxOffice?: string
    production?: string
    website?: string
}

export default class MovieController {

    private api: OmdbMovieApi

    constructor(apiKey: string) {
        this.api = new OmdbMovieApi(apiKey);
    }

    public async getDefaultMovieList(): Promise<MovieSnippet[]> {
        const currentYear = (new Date()).getFullYear()
        return this.searchMovies("legend", currentYear)
    }

    public async searchMovies(title: string, year?: number): Promise<MovieSnippet[]> {
        const movieData = await this.api.retrieveMovieByTitleSearch(title, year);
        const movies: MovieSnippet[] = movieData.Search.map(m => ({
            id: m.imdbID,
            title: m.Title,
            year: m.Year,
            posterUrl: m.Poster
        }));
        return movies
    }

    public async getMovieDetail(movieId: string): Promise<Movie> {
        const movieData = await this.api.retrieveMovieById(movieId);
        const movie: Movie = {
            title: movieData.Title,
            year: movieData.Year,
            parentalAdvice: movieData.Rated,
            duration: movieData.Runtime,
            genre: movieData.Genre,
            director: movieData.Director,
            writer: movieData.Writer,
            actors: movieData.Actors,
            plot: movieData.Plot,
            language: movieData.Language,
            country: movieData.Country,
            posterUrl: movieData.Poster,
            metascore: movieData.Metascore,
            rating: movieData.imdbRating,
            id: movieData.imdbID,
            dvd: movieData.DVD,
            boxOffice: movieData.BoxOffice,
            production: movieData.Production,
            website: movieData.Website,
        }
        return movie
    }
}



// function getMovieByTitle(titleSearchTerm: String): Movie[] {

// }