import axios from 'axios';
import { Movie } from './MovieController';

export interface BaseOmdbMovieDetail {
    Title: string
    Year: string
    imdbID: string
    Type: string
    Poster: string
}

export interface OmdbMovie {
    Title: string
    Year: string
    Rated: string
    Runtime: string
    Genre: string
    Director: string
    Writer: string
    Actors: string
    Plot: string
    Language: string
    Country: string
    Poster: string
    Ratings: Rating[]
    Metascore: string
    imdbRating: string
    imdbVotes: string
    imdbID: string
    Type: string
    DVD?: string
    BoxOffice?: string
    Production?: string
    Website?: string
}

interface Rating {
    Source: string
    Value: string
}

interface OmdbMovieSearchResult {
    Search: BaseOmdbMovieDetail[]
}

const OMDB_MOVIE_API_BASE_URL = "www.omdbapi.com"

export default class OmdbMovieApi {
    
    apiKey: string
    baseApiUrl: string
    
    constructor(apiKey: string) {
        this.apiKey = apiKey
        this.baseApiUrl = this.buildApiUrl(apiKey)
    }

    private buildApiUrl(apiKey: string) {
        return `http://${OMDB_MOVIE_API_BASE_URL}/?apikey=${apiKey}&type=movie`
    }

    public async retrieveMovieById(
        id: string
    ):Promise<OmdbMovie> {
        const url = `${this.baseApiUrl}&i=${id}`
        return await axios.get(url)
            .then(resp => {
                if(resp.data.Response && resp.data.Response == "False") {
                    if(resp.data.Error)
                        return Promise.reject(resp.data.Error)
                } else return resp.data
            });
    }

    public async retrieveMovieByTitleSearch(
        searchTerm: string,
        year?: number,
    ):Promise<OmdbMovieSearchResult> {
        const yearSearchString = (year)? `&y=${year}` : ""
        const url = `${this.baseApiUrl}&s=${searchTerm}${yearSearchString}`
        return await axios.get(url)
            .then(resp => {
                if(resp.data.Response && resp.data.Response == "False") {
                    if(resp.data.Error)
                        return Promise.reject(resp.data.Error)
                } else return resp.data
            });
    }
    


}
