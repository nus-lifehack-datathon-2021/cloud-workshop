import MovieController from '../controllers/MovieController'

describe('movie search using omdb api', () => {
    it('validate api endpoint and should fail with invalid api key supplied', async () => {
        expect.assertions(1);
        const apiKey = "invalid_key";
        const controller = new MovieController(apiKey);
        return controller.getDefaultMovieList().catch(e =>
             expect(e.response.status).toEqual(401)
        );
    });

    it('retrieval of default movie list is of current year', async () => {
        // retrieve api key from os environment
        const apiKey: string = String(process.env.API_KEY)

        // retrieve and check
        const controller = new MovieController(apiKey);
        const movies = await controller.getDefaultMovieList();
        expect(movies.every(m => m.year === String((new Date).getFullYear()))).toBe(true)
    });

})