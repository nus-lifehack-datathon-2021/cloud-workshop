import '@elastic/eui/dist/eui_theme_amsterdam_dark.css';

import React, { useState, useEffect } from 'react';

import {
  EuiPage,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageContentHeaderSection,
  EuiTitle,
} from '@elastic/eui';

import MovieAppHeader from './components/Header'
import MovieList from './components/MovieList'
import MovieApi, { MovieSnippet } from './api/MovieApi'

function App() {

  // initial list of movies (snippets)
  const emptyMovieList: MovieSnippet[] = []
  const [movieSnippets, setMovieSnippets] = useState(emptyMovieList);
  const [apiError, setApiError] = useState("");

  const onMovieSearch = (searchTerm: string) => {
    MovieApi.searchMovies(searchTerm, ((results, error) => {
      if(error) {
        setApiError(`There is an issue with the backend. ${error.message}`);
      } else {
        setApiError("")
      }
      setMovieSnippets(results)
    }));
  }

  useEffect(() => {
    MovieApi.retrieveDefaultMovieList((movies, error) => {
      if(error) {
        setApiError(`There is an issue with the backend. ${error.message}`);
      } else {
        setApiError("")
      }
      setMovieSnippets(movies);
    });
  }, []);

  return (
    <EuiPage>
      <EuiPageBody component="div">
        <EuiPageContent verticalPosition="center" horizontalPosition="center">
        <EuiPageContentHeader>
            <EuiPageContentHeaderSection>
              <EuiTitle>
                <MovieAppHeader onMovieSearch={onMovieSearch} />
              </EuiTitle>
            </EuiPageContentHeaderSection>
          </EuiPageContentHeader>
          <EuiPageContentBody>
            { (movieSnippets && movieSnippets.length > 0)? 
                <MovieList items={movieSnippets} /> :
                "No result found. " 
            } 
            { apiError }
          </EuiPageContentBody>
        </EuiPageContent>
      </EuiPageBody>
    </EuiPage>
  );
}

export default App;
