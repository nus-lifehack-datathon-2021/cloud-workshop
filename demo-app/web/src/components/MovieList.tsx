
import React, { FunctionComponent, useState } from 'react';

import {
  EuiCard,
  EuiFlexGroup,
  EuiFlexItem,
} from '@elastic/eui';

import MovieDetailFlyout from './MovieDetailFlyout'
import MovieApi, { MovieSnippet } from '../api/MovieApi';

const MovieList: FunctionComponent<{ 
  items: MovieSnippet[],
}> = ({ items }) => {

  const [movieDetail, setMovieDetail] = useState(MovieApi.emptyMovieDetail);
  const [isMovieDetailShown, showMovieDetail] = useState(false);

  const handleOpenMovieDetail = (movieId: string) => {
    MovieApi.retrieveMovieDetail(movieId, (movie, error) => {

      if(error) {
        showMovieDetail(false);
        console.error('Unable to retrieve movie details');
      } else {
        setMovieDetail(movie);
        showMovieDetail(true);
      }
    }) 
  }

  const handleFlyoutClose = () => {
    showMovieDetail(false);
  }

  const movieCards = items.map(function(item, index) {
    return (
      <EuiFlexItem key={item.id} style={{ minWidth: 150, maxWidth: 150 }}>
        <EuiCard
          textAlign="left"
          image={(item.posterUrl !== "N/A")? item.posterUrl : "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg" }
          title=""
          description={item.title}
          onClick={() => handleOpenMovieDetail(item.id) }
        />
      </EuiFlexItem>
    );
  });

    return (
        <>
          <EuiFlexGroup 
              style={{ maxWidth: 900 }} 
              gutterSize="l" wrap>
              {movieCards}
          </EuiFlexGroup>
        <MovieDetailFlyout 
          showMovieDetail={isMovieDetailShown} 
          movie={movieDetail} 
          onClose={handleFlyoutClose} />
        </>
    )
}

export default MovieList;