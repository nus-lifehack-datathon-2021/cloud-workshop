import React, { FunctionComponent } from 'react';

import {
    EuiFlyout,
    EuiFlyoutBody,
    EuiFlyoutHeader,
    EuiDescriptionList,
    EuiTitle,
    EuiImage,
    EuiSpacer,
} from '@elastic/eui'
import { Movie } from '../api/MovieApi';

const MovieDetailFlyout: FunctionComponent<{ 
    movie: Movie,
    showMovieDetail: boolean,
    onClose: () => void,
  }> = ({ 
      movie, 
      showMovieDetail, 
      onClose }) => {
    
    const movieDetail = [
        {
            title: "Genre",
            description: movie.genre,
        },
        {
            title: "Year",
            description: movie.year,
        },
        {
            title: "Parental Advice",
            description: movie.parentalAdvice,
        },
        {
            title: "Length",
            description: movie.duration,
        },
        {
            title: "Directors",
            description: movie.director,
        },
        {
            title: "Writers",
            description: movie.writer,
        },
        {
            title: "Actors",
            description: movie.actors,
        },
        {
            title: "Rating",
            description: movie.rating,
        },
        {
            title: "Description",
            description: movie.plot,
        }
    ]

    let flyout;
    if(showMovieDetail) {
        flyout = (
            <EuiFlyout
                ownFocus
                onClose={() => onClose() }>
                <EuiFlyoutHeader hasBorder>
                    <EuiTitle size="m">
                        <h2>{movie.title}</h2>
                    </EuiTitle>
                </EuiFlyoutHeader>
                <EuiFlyoutBody>
                    <EuiImage
                        alt={movie.title}
                        src={movie.posterUrl}
                    />
                    <EuiSpacer />
                    <EuiDescriptionList listItems={movieDetail} />
                </EuiFlyoutBody>
            </EuiFlyout>
        )
    }
    return (
        <div>{flyout}</div>
    );
}

export default MovieDetailFlyout