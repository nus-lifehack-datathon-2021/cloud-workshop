import React, { FunctionComponent } from 'react';

import {
    EuiHeader,
    EuiHeaderLogo,
    EuiHeaderSections,
    EuiFieldSearch,    
} from '@elastic/eui'

const Header: FunctionComponent<{ 
    onMovieSearch: (term: string) => void,
}> = ({ onMovieSearch }) => {

    const search = (
        <EuiFieldSearch 
            placeholder="Movies" 
            compressed
            incremental={false}
            onSearch={(term) => { onMovieSearch(term) }} />
    )

    const sections: EuiHeaderSections[] = [
        {
            items: [
                <EuiHeaderLogo
                    iconType="logoKibana"
                    href="#"
                    aria-label="Movies" />,
                'Movies'
            ],
            borders: 'right',
        },
        {
            items: [
                search
            ],
            borders: 'none',
        },
    ]

    return <EuiHeader position="fixed" sections={sections} />
  }

export default Header;