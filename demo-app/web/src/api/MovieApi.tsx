
export interface MovieSnippet {
    title: string
    year: string
    id: string
    posterUrl: string
}

export interface Movie {
    title: string
    year: string
    parentalAdvice: string
    duration: string
    genre: string
    director: string
    writer: string
    actors: string
    plot: string
    language: string
    country: string
    posterUrl: string
    metascore: string
    rating: string
    id: string
    dvd?: string
    boxOffice?: string
    production?: string
    website?: string
}

const emptyMovieDetail = {
    title: "",
    year: "",
    parentalAdvice: "",
    duration: "",
    genre: "",
    director: "",
    writer: "",
    actors: "",
    plot: "",
    language: "",
    country: "",
    posterUrl: "",
    metascore: "",
    rating: "",
    id: "",
  }

function retrieveDefaultMovieList(
    handler: (movies: MovieSnippet[], error?: any) => void) {

    fetch("/api/movie")
      .then(resp => {
        if(!resp.ok) {
          throw Error(resp.statusText);
        }
        return resp.json()
      })
      .then(data => {
        const movies = data as MovieSnippet[]
        handler(movies);
      })
      .catch(error => {
        handler([], error);
      });
}

function retrieveMovieDetail(movieId: string,
    handler: (movie: Movie, error?: Error) => void) {
    
    fetch(`/api/movie/${movieId}`)
      .then(resp => {
        if(!resp.ok) {
          throw Error(resp.statusText);
        }
        return resp.json()
      })
      .then(data => {
        const movie = data as Movie
        handler(movie)
      })
      .catch(error => {
        handler(emptyMovieDetail, error);
      });
}

function searchMovies(title: string, 
    handler: (movies: MovieSnippet[], error?: Error) => void) {

    fetch(`/api/movie?search=${title}`)
      .then(resp => {
        if(!resp.ok) {
          throw Error(resp.statusText);
        }
        return resp.json()
      })
      .then(data => {
        const movies = data as MovieSnippet[]
        handler(movies);
      })
      .catch(error => {
        handler([], error);
      });
}

const MovieApi = {
    retrieveMovieDetail,
    retrieveDefaultMovieList,
    searchMovies,
    emptyMovieDetail,
}

export default MovieApi;