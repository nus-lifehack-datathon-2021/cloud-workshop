
# Infrastructure Setup

## Cloud Services

We will be leveraging the following cloud service in AWS to run our demo app.

### Amazon EKS (Elastic Kubernetes Services)

This is a managed kubernetes service in AWS. It is easy to provision a 
full blown kubernetes cluster and have Amazon manages the cluster for us. 

This gives us these benefits:

- High availability (HA) kubernetes setup
- Load Balancing capability
- Identity and Access Control (Automated integration with AWS IAM)
- TLS Certificate Management

To interact with EKS, we will be using `eksctl`, a command line utility to manage clusters.

