# Container Orchestration

Both `api service` and `web frontend` will be deploy on Amazon EKS cluster, a managed kubernetes service. See [here](./4-infra.md) for the setup

Kubernetes deployment is done by crafting Kubernetes manifests in YAML. These manifests specification of how the application containers are structured or lay out. 

Manifest declarative describe the `expected state` of the application and the kubernetes engine will do the rest to make the `current state` to match the `expected state`.

The kubernetes manifests for the demo app is in [deployment](./deployment) folder. 

We use `kubectl`, a CLI tool to interact with a kubernetes cluster.

```bash
kubectl apply -f backend-api.yaml
```

## Accessing EKS

To access the kubernetes cluster using `kubectl`, a `kubeconfig` file at $HOME/.kube/config is used. 

It define how to locate to cluster and authenticate to it.

For EKS, run the command below to buld the `kubeconfig` file

```bash
# get the kubeconfig file
eksctl utils write-kubeconfig --cluster nus-lifehack --region ap-southeast-1

# verify cluster is accessible
kubectl cluster-info
``` 

## Pod

A pod is almost an equivalent to a container. It is a single instance or replica of the application. 

Below is a pod definition for the `web frontend`

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: webapp
spec: 
  containers:
  - name: webapp
    image: registry.gitlab.com/nus-lifehack-datathon-2021/cloud-workshop/web:latest
    ports: 
    - containerPort: 80
```

To see pods that are running

```bash
kubectl get pod

NAME                           READY   STATUS    RESTARTS   AGE
webapp-8689d9f557-hsgj4        1/1     Running   0          27h
webapp-8689d9f557-smp5f        1/1     Running   0          27h
webapp-8689d9f557-wzssn        1/1     Running   0          27h

# see logs of application (console output from stdout/stderr)
kubectl logs webapp-8689d9f557-hsgj4 
```

## Deployment

Rather than defining a `pod` individually, it is more common to define a `deployment`.

A `deployment` will start the `pods` and manages their numbers. 

Deployment also has the advantage that kubernetes will automatically create new pod if any existing pod crashes or deleted

```yaml
apiVersion: apps/v1
kind: Deployment
metadata: 
  name: webapp
  labels:
    app: webapp
spec:
  replicas: 3
  selector:
    matchLabels:
      app: webapp
  template:
    metadata:
      labels:
        app: webapp
    spec: 
      containers:
      - name: webapp
        image: registry.gitlab.com/nus-lifehack-datathon-2021/cloud-workshop/web:latest
        ports: 
        - containerPort: 80
```

To see a deployment

```
kubectl get deploy webapp

NAME     READY   UP-TO-DATE   AVAILABLE   AGE
webapp   3/3     3            3           30h
```

## Service 

To expose the deployment, we need to create clusterIP `service` and `ingress`

Service expose the `pods`. After defining a `service`, kubernetes will gives each pod their own internal IP addresses and a `single DNS name`. It also `load balance` the pods.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: webapp
spec:
  selector:
    app: webapp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

To see a service

```
kubectl get svc webapp

NAME     TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
webapp   ClusterIP   10.100.80.44   <none>        80/TCP    30h

kubectl get endpoints webapp

NAME     ENDPOINTS                                           AGE
webapp   192.168.54.133:80,192.168.81.73:80,192.168.9.9:80   30h

```

Note the single virtual IP assigned to the service and the dns name will be `webapp.svc.cluster.local`

## Ingress

ClusterIP `service` is only reachable within the kubernetes cluster. 

To able end users to access the application, we need to define ingress, which describe the routing rules to reach these services.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: movie-app-ingress
  annotations:
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "false"
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: webapp
            port:
              number: 80
      - path: /api
        pathType: Prefix
        backend:
          service:
            name: backend-api
            port:
              number: 3000

```

## Next Step

Go to [cloud infrastructure & infra as code](./4-infra.md) or [CI/CD pipeline](./5-pipeline.md).