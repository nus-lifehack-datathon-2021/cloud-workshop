# Step 0: Prerequisites

The entire demo will run on `AWS cloud9` and `gitlab.com`. Running the demo on local machine is possible but preferably on a Linux based machine.

The following is needed:

- [Gitlab account](https://gitlab.com)
- [Amazon AWS account](https://aws.amazon.com/console)

## Development tools and clis 

To run the web app, the following is needed on the development machine:

- [Git](https://git-scm.com/)
- [Node.js and NPM](https://nodejs.dev/)
- [Yarn](https://yarnpkg.com/getting-started/install)
- [Docker or Docker Desktop](https://www.docker.com/products/docker-desktop)

AWS cloud9 is already bundled with a number of commonly used development toolings, but their version might be dated. On cloud9, do the following to ensure the toolings are in-place. 

```bash
# nvm is great for managing node.js versioning, and cloud9 came with it
# use nvm to upgrade to latest node.js version
nvm install node

# install yarn
npm install -g yarn

# check docker is installed and its version
docker version
```

For interacting with aws and kubernetes, the following is needed. 

- [eksctl](https://eksctl.io/)
- [aws cli](https://aws.amazon.com/cli/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)


## Next Step

Go to [setup](./1-setup.md). 