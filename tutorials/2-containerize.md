# Step 2: Containerize the App

To leverage on kubernetes or Amazon EKS, the apps need to be containerized. We will build individual container image for each service.

## Docker and Container Concept

To build container image, we need to craft `Dockerfile` for each image. A `Dockerfile` functions as a build script for creating the image.

**Base Image and Layer concept**

First, all container image start off with a `base image`. The base image contains the operating system (OS) and typically all the base libraries and toolings that your application requires.

For example, to run our demo api service, which is a node.js application, we would need `node.js` to be available. Thus, we pick to use `node:14.17-alpine3.14` as the base.

Below is how the base image is defined in `Dockerfile`

```Dockerfile
# Use official node.js v14
FROM node:14.17-alpine3.14
```

After deciding on a base image, we need to add any other things or configuration that we need to run the application. Each of these are typically `linux shell command` for a linux based container.

Each of these instructions become a **container layer** on top of the base image. 

![Docker container layering concept](https://miro.medium.com/max/700/1*ZyDop_nTtu26CVpz5uFQUg.png)

(image from https://towardsdatascience.com/docker-storage-598e385f4efe)

Below show an example of such instruction. One typical use is also to build the application binary.

```Dockerfile
# install dumb-init. apk is the package manager in alpine os. similar to yum or apt for centos or ubuntu
RUN apk add dumb-init
```

**Environment Variables**

Following [12-factor](https://12factor.net/) design principles, app configuration is done setting OS environment variables in the container. We can specifies default environment variables in the `Dockerfile`

```Dockerfile
# Environment variables (optimized for production)
ENV NODE_PATH=./build
ENV API_KEY=""  
ENV PORT="3000"
```

**Application Binary**

To copy your application binary or sources to the the image, use the `COPY` instruction.

```Dockerfile
# Specify app directory and install dependencies in it
WORKDIR /home/node/app

# Copy all api module codes into container
COPY --chown=node:node . ./

# build api service
RUN yarn
RUN yarn build
```

**Application Execution**

The last instruction in the `Dockerfile` is `CMD` and/or `ENTRYPOINT`

`ENTRYPOINT` defines the **default command** that runs when the container is started. For example, `bash -c`

`CMD` instruction is to provide default parameters to the default command defined in `ENTRYPOINT`.

```Dockerfile
ENTRYPOINT ["docker-entrypoint.sh"]

# main command to run in the container 
CMD [ "dumb-init", "node", "build/index.js" ]
```

## Container Image Build

On local machine, run the below docker command to build your container image

```bash
# build the image
docker build -t registry.gitlab.com/nus-lifehack-datathon-2021/cloud-workshop/api:v1.0.0

# push the image to registry.gitlab.com (container registry) 
docker push registry.gitlab.com/nus-lifehack-datathon-2021/cloud-workshop/api:v1.0.0
```

For production, it is **not recommended**  to run docker build command especially on DIND (Docker on Docker container). Docker build requires exposing docker daemon, which is a security risk since docker daemon has OS kernal privilege access.

To build container on container, use a daemon-less tool such as [Kaniko](https://github.com/GoogleContainerTools/kaniko) from Google.

```bash
# Inside a kaniko container:
/kaniko/executor \
      --cache \
      --context $CI_PROJECT_DIR/demo-app/api \
      --dockerfile $CI_PROJECT_DIR/demo-app/api/Dockerfile \
      --destination registry.gitlab.com/nus-lifehack-datathon-2021/cloud-workshop/api:v1.0.0
```

## Container Run 

To run container on local machine

```bash
docker run \
  -d -e PORT=3000 \
  registry.gitlab.com/nus-lifehack-datathon-2021/cloud-workshop/api:v1.0.0 \ 
  movie-api-service  

# list running containers
docker ps
```

## Next Step

Go to [container orchestration](./3-container-orchestration.md).

