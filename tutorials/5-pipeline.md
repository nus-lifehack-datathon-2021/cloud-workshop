## CI/CD Pipeline

We will explore building CI/CD pipeline using `Gitlab CI` on [gitlab.com](https://gitlab.com)

There are many CI/CD tools such a Github Actions, Circle CI, Jenkins, etc.

In Gitlab CI pipeline is defined in `.gitlab-ci.yml` file using YAML. General structure is as below

```yaml
# CI variables 
# Become environment variables when CI job runs
variables:
  ...VAR_1: value

# CI/CD pipeline stages
stages:
  - test
  - build
  - deploy
  
# CI job templates
.kaniko_build_template:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  before_script:
    - linux command...

# CI Jobs

test:api:
  stage: test
  image: node:14
  script:
    - linux command...

build:api:
  extends: .kaniko_build_template
  stage: build
  script:
    - linux command...
```

Pipline is split into multiple `stages`. Each stage can have any number of CI `jobs` run concurrently. 

For each job, GitLab CI will create a new container to execute a run. Thus, residue from previous job will not affect current job.

