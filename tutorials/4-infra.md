
## Cloud Infrastructure & Infra as Code

### Amazon EKS (Elastic Kubernetes Services)

This is a managed kubernetes service in AWS. It is easy to provision a 
full blown kubernetes cluster and have Amazon manages the cluster for us. 

This gives us these benefits:

- High availability (HA) kubernetes setup
- Load Balancing capability
- Identity and Access Control (Automated integration with AWS IAM)
- TLS Certificate Management

To interact with EKS, we will be using [eksctl](https://eksctl.io/), a command line utility to manage clusters.

### Caveats

Do note that EKS is not part AWS free tier. Every hour of use will incur $0.10 for each cluster created

## Note on Simple Setup

For simplicity, we will run the setup as an IAM user with administrator access. In production, you will almost alway run using a user that has just enough privilege. 

Note down the following account details. They will be needed shortly

- AWS account ID
- Access Key of the IAM user
  - Key ID
  - Access Secret Key 

On cloud9 or EC2 instance, Amazon handles the role based access control (RBAC) using IAM for us automatically.

On other machine, you will need to supply the account ID and access key as environment variables

```bash

export AWS_ACCOUNT_ID=<AWS Account ID>
export AWS_ACCESS_KEY_ID=<AWS Access Key ID>
export AWS_SECRET_ACCESS_KEY=<AWS Access Key>

# Singapore
export AWS_REGION=ap-southeast-1

# Install aws cli if not available
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

unzip awscliv2.zip
sudo ./aws/install

# List login details using aws CLI
aws sts get-caller-identity --output text
```


## EKS Cluster Creation

EKS can be easily provisioned using `eksctl`. The cluster configuration is specified in a yaml manifest that is passed to the tool.

First, craft the cluster configuration file `eks_cluster.yaml`

```
---
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: nus-lifehack
  region: ap-southeast-1        
  version: "1.20"

availabilityZones: [
  "ap-southeast-1a", 
  "ap-southeast-1b", 
  "ap-southeast-1c"
]

managedNodeGroups:
  - name: nodegroup
    desiredCapacity: 3
    instanceType: t3.small
    ssh:
      enableSsm: true
```

Then, run the below command to create the cluster. It may take 15-25 mins for Amazon to build the cluster.

```bash
# provision the EKS cluster
eksctl create cluster -f eks_cluster.yaml

# get the kubeconfig file
eksctl utils write-kubeconfig --cluster=nus-lifehack

# check the cluster by listing the kubernetes nodes or machines
kubectl get nodes
```

As seen,  `eks_cluster.yaml` is a form of an infrastructure code.

Typically, this is also run over a CI/CD pipeline. Changes committed to a git repo will trigger a CI job to call `eksctl`. This style of managing infrastructure is also known as **GitOps**. 

## Scale Cluster

To scale cluster, modify `eks_cluster.yaml` to set the desired capacity of the managed node group from 3 to 4.

```yaml
nodeGroups:
  - name: group_a
    minSize: 3
    maxSize: 9
    desiredCapacity: 4        # previous is 3
    instanceType: t3.small
    ssh:
      enableSsm: true
```

Apply the updated configuration over `eksctl`

```bash
cat eks_cluster.yaml | sed "s/{{CLUSTER_NAME}}/nus-lifehack/g" | eksctl scale nodegroup --name nodegroup -f -
```

## EKS Cluster Configuration - Enable Ingress

Ingress define the routing rules to expose your apps externally. It need an ingress controller to functions and 
that is not out of the box with a new cluster creation. Thus, we need to add an ingress controller before using ingress.

Here, we will use nginx as the ingress controller. To install that, run:

```bash
# deploy ingress controller and network load balancer
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/aws/deploy.yaml
```


## Additional Resources

To learn more, you can visit these links below to learn more:

- https://eksctl.io/
- [https://www.eksworkshop.com](https://www.eksworkshop.com/)

## Next Step

Go to [CI/CD pipeline](./5-pipeline.md).
