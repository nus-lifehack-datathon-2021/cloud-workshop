# Step 1: Setup

Fork from this git repository into a separate git project in `gitlab.com`. We will be using gitlab CI to run our CI/CD pipeline.

After fork, **clone the git repo** in the development machine.

```bash
git clone git@gitlab.com:nus-lifehack-datathon-2021/cloud-workshop.git
```


## Run the app

In this step, we will familarize with the app by runing the `web` and `app` on the development machine.

To run the `api service`, we need to configure the service by setting the environment variables first

- **PORT** - The network port that the api service will listen/run on
- **API_KEY** - api service is a wrapper around [OMdb Movie api](https://www.omdbapi.com/). The service needs the api key for accessing it

```bash
# Configure the api service by setting the environment variables 
# alternatively, put them in demo-app/api/.env
export PORT=3000
export API_KEY=<omdb movie api key>
```



```bash
# navigate to th root directory of the api service
cd demo-app/api

# pull dependencies or node modules
yarn

# start the express server and use nodemon to monitor code changes
yarn dev
```

To run the `web frontend`

```bash
# navigate to th root directory of the web frontend
cd demo-app/web

# pull dependencies or node modules
yarn

# start the web frontend
yarn start
```

## Next Step

Go to [containerize app](./2-containerize.md). 
