# Operationalizing Apps at Scale in the Cloud

This is a tutorial workshop on deploying apps at scale using cloud native toolings such as Kubernetes and Containers on Amazon AWS.

This talk was given at NUS LifeHack 2021 on 22 July 2021.


## Demo App

The workshop will use a `Movie Database` app (similar to IMDb) to demonstrate the various concepts.

The app is a typical **single page web based appliation** built using `React`, `Node.js` and `typescript`. We will see how to `containerize` the app and run it at scale using `Amazon EKS (Elastic Kubernetes Service)`. 

The EKS will be provisioned using infra as code, and both infra and app code is run over CI/CD pipelines in `gitlab.com`.  

Below depicts how the app is structured

![demo app](./tutorials/images/sample-app.png)



## Walkthrough

1. [Prerequisites](./tutorials/0-prerequisites.md)
2. [The Setup](./tutorials/1-setup.md)
3. [Containerize App](./tutorials/2-containerize.md)
4. [Container Orchestration](./tutorials/3-container-orchestration.md)
5. [Cloud Infrastructure & Infra as Code](./tutorials/4-infra.md)
6. [CI/CD pipeline](./tutorials/5-pipeline.md)
